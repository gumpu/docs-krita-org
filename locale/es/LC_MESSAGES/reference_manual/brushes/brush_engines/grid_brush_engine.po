# Spanish translations for docs_krita_org_reference_manual___brushes___brush_engines___grid_brush_engine.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___grid_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-19 22:14+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Mix with background color"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:1
msgid "The Grid Brush Engine manual page."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:17
msgid "Grid Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:12
msgid "Brush Engine"
msgstr "Motor de pinceles"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:12
msgid "Grid"
msgstr "Cuadrícula"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:21
msgid ".. image:: images/icons/gridbrush.svg"
msgstr ".. image:: images/icons/gridbrush.svg"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:22
msgid ""
"The grid brush engine draws shapes on a grid. It helps you produce retro and "
"halftone effects."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:24
msgid ""
"If you're looking to setup a grid for snapping, head to :ref:"
"`grids_and_guides_docker`."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:27
msgid "Options"
msgstr "Opciones"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:29
msgid ":ref:`option_size_grid`"
msgstr ":ref:`option_size_grid`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:30
msgid ":ref:`option_particle_type`"
msgstr ":ref:`option_particle_type`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:31
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:32
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:33
msgid ":ref:`option_color_grid`"
msgstr ":ref:`option_color_grid`"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:38
msgid "Brush Size"
msgstr "Tamaño del pincel"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:40
msgid "Grid Width"
msgstr "Anchura de la cuadrícula"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:41
msgid "Width of the cursor area."
msgstr "Anchura del área del cursor."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:42
msgid "Grid Height"
msgstr "Altura de la cuadrícula"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:43
msgid "Height of the cursor area."
msgstr "Altura del área del cursor."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:44
msgid "Division"
msgstr "División"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:45
msgid ""
"Subdivides the cursor area and uses the resulting area to draw the particles."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:46
msgid "Division by pressure"
msgstr "División por presión"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:47
msgid ""
"The more you press, the more subdivisions. Uses Division as the finest "
"subdivision possible."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:48
msgid "Scale"
msgstr "Escalar"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:49
msgid "Scales up the area."
msgstr "Escala el área."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:50
msgid "Vertical Border"
msgstr "Borde vertical"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:51
msgid ""
"Forces vertical borders in the particle space, between which the particle "
"needs to squeeze itself."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:52
msgid "Horizontal Border"
msgstr "Borde horizontal"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:53
msgid ""
"Forces a horizontal borders in the particle space, between which the "
"particle needs to squeeze itself."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:55
msgid "Jitter Borders"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:55
msgid "Randomizes the border values with the Border values given as maximums."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:60
msgid "Particle Type"
msgstr "Tipo de partícula"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:62
msgid "Decides the shape of the particle."
msgstr "Decide la forma de la partícula."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:64
msgid "Ellipse"
msgstr "Elipse"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:65
msgid "Fills the area with an ellipse."
msgstr "Rellena el área con una elipse."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:66
msgid "Rectangle"
msgstr "Rectángulo"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:67
msgid "Fills the area."
msgstr "Rellena el área."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:68
msgid "Line"
msgstr "Línea"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:69
msgid ""
"Draws lines from the lower left to the upper right corner of the particle."
msgstr ""
"Dibuja líneas desde la esquina inferior izquierda hasta la superior derecha "
"de la partícula."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:70
msgid "Pixel"
msgstr "Píxel"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:71
msgid "Looks like an aliased line on high resolutions."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:73
msgid "Anti-aliased Pixel"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:73
msgid "Fills the area with little polygons."
msgstr "Rellena el área con polígonos pequeños."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:78
msgid "Color Options"
msgstr "Opciones de color"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:80
msgid "Random HSV"
msgstr "HSV aleatorio"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:81
msgid ""
"Randomize the HSV with the strength of the sliders. The higher, the more the "
"color will deviate from the foreground color, with the direction indicating "
"clock or counter clockwise."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:82
msgid "Random Opacity"
msgstr "Opacidad aleatoria"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:83
msgid "Randomizes the opacity."
msgstr "Usa una opacidad aleatoria."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:84
msgid "Color Per Particle"
msgstr "Color por partícula"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:85
msgid "Has the color options be per particle instead of area."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:86
msgid "Sample Input Layer"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:87
msgid ""
"Will use the underlying layer as reference for the colors instead of the "
"foreground color."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:88
msgid "Fill Background"
msgstr "Rellenar el fondo"

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:89
msgid "Fills the area before drawing the particles with the background color."
msgstr "Rellena el área con el color de fondo antes de dibujar las partículas."

#: ../../reference_manual/brushes/brush_engines/grid_brush_engine.rst:91
msgid ""
"Gives the particle a random color between foreground/input/random HSV and "
"the background color."
msgstr ""
