# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-05-07 10:57+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../user_manual/snapping.rst:1
msgid "How to use the snapping functionality in Krita."
msgstr "Hoe de functie aanklikken in Krita gebruiken."

#: ../../user_manual/snapping.rst:10 ../../user_manual/snapping.rst:43
msgid "Guides"
msgstr "Hulplijnen"

#: ../../user_manual/snapping.rst:10
msgid "Snap"
msgstr "Vastklikken"

#: ../../user_manual/snapping.rst:10
msgid "Vector"
msgstr "Vector"

#: ../../user_manual/snapping.rst:15
msgid "Snapping"
msgstr "Aanklikken"

#: ../../user_manual/snapping.rst:17
msgid ""
"In Krita 3.0, we now have functionality for Grids and Guides, but of course, "
"this functionality is by itself not that interesting without snapping."
msgstr ""

#: ../../user_manual/snapping.rst:21
msgid ""
"Snapping is the ability to have Krita automatically align a selection or "
"shape to the grids and guides, document center and document edges. For "
"Vector layers, this goes even a step further, and we can let you snap to "
"bounding boxes, intersections, extrapolated lines and more."
msgstr ""

#: ../../user_manual/snapping.rst:26
msgid ""
"All of these can be toggled using the snap pop-up menu which is assigned to :"
"kbd:`Shift + S` shortcut."
msgstr ""

#: ../../user_manual/snapping.rst:29
msgid "Now, let us go over what each option means:"
msgstr ""

#: ../../user_manual/snapping.rst:32
msgid ""
"This will snap the cursor to the current grid, as configured in the grid "
"docker. This doesn’t need the grid to be visible. Grids are saved per "
"document, making this useful for aligning your art work to grids, as is the "
"case for game sprites and grid-based designs."
msgstr ""

#: ../../user_manual/snapping.rst:34
msgid "Grids"
msgstr ""

#: ../../user_manual/snapping.rst:37
msgid "Pixel"
msgstr "Pixel"

#: ../../user_manual/snapping.rst:37
msgid ""
"This allows to snap to every pixel under the cursor. Similar to Grid "
"Snapping but with a grid having spacing = 1px and offset = 0px."
msgstr ""

#: ../../user_manual/snapping.rst:40
msgid ""
"This allows you to snap to guides, which can be dragged out from the ruler. "
"Guides do not need to be visible for this, and are saved per document. This "
"is useful for comic panels and similar print-layouts, though we recommend "
"Scribus for more intensive work."
msgstr ""

#: ../../user_manual/snapping.rst:46
msgid ".. image:: images/snapping/Snap-orthogonal.png"
msgstr ""

#: ../../user_manual/snapping.rst:48
msgid ""
"This allows you to snap to a horizontal or vertical line from existing "
"vector objects’s nodes (Unless dealing with resizing the height or width "
"only, in which case you can drag the cursor over the path). This is useful "
"for aligning object horizontally or vertically, like with comic panels."
msgstr ""

#: ../../user_manual/snapping.rst:52
msgid "Orthogonal (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:55
msgid ".. image:: images/snapping/Snap-node.png"
msgstr ""

#: ../../user_manual/snapping.rst:57
msgid "Node (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:57
msgid "This snaps a vector node or an object to the nodes of another path."
msgstr ""

#: ../../user_manual/snapping.rst:60
msgid ".. image:: images/snapping/Snap-extension.png"
msgstr ""

#: ../../user_manual/snapping.rst:62
msgid ""
"When we draw an open path, the last nodes on either side can be "
"mathematically extended. This option allows you to snap to that. The "
"direction of the node depends on its side handles in path editing mode."
msgstr ""

#: ../../user_manual/snapping.rst:65
msgid "Extension (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:68
msgid ".. image:: images/snapping/Snap-intersection.png"
msgstr ""

#: ../../user_manual/snapping.rst:69
msgid "Intersection (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:70
msgid "This allows you to snap to an intersection of two vectors."
msgstr ""

#: ../../user_manual/snapping.rst:71
msgid "Bounding box (Vector Only)"
msgstr ""

#: ../../user_manual/snapping.rst:72
msgid "This allows you to snap to the bounding box of a vector shape."
msgstr ""

#: ../../user_manual/snapping.rst:74
msgid "Image bounds"
msgstr "Grenzen van afbeelding"

#: ../../user_manual/snapping.rst:74
msgid "Allows you to snap to the vertical and horizontal borders of an image."
msgstr ""

#: ../../user_manual/snapping.rst:77
msgid "Allows you to snap to the horizontal and vertical center of an image."
msgstr ""

#: ../../user_manual/snapping.rst:78
msgid "Image center"
msgstr "Centrum van afbeelding"

#: ../../user_manual/snapping.rst:80
msgid "The snap works for the following tools:"
msgstr ""

#: ../../user_manual/snapping.rst:82
msgid "Straight line"
msgstr "Rechte lijn"

#: ../../user_manual/snapping.rst:83
msgid "Rectangle"
msgstr "Rechthoek"

#: ../../user_manual/snapping.rst:84
msgid "Ellipse"
msgstr "Ellips"

#: ../../user_manual/snapping.rst:85
msgid "Polyline"
msgstr "Gebroken lijn"

#: ../../user_manual/snapping.rst:86
msgid "Path"
msgstr "Pad"

#: ../../user_manual/snapping.rst:87
msgid "Freehand path"
msgstr "Pad uit de vrije hand"

#: ../../user_manual/snapping.rst:88
msgid "Polygon"
msgstr ""

#: ../../user_manual/snapping.rst:89
msgid "Gradient"
msgstr "Kleurverloop"

#: ../../user_manual/snapping.rst:90
msgid "Shape Handling tool"
msgstr ""

#: ../../user_manual/snapping.rst:91
msgid "The Text-tool"
msgstr ""

#: ../../user_manual/snapping.rst:92
msgid "Assistant editing tools"
msgstr ""

#: ../../user_manual/snapping.rst:93
msgid ""
"The move tool (note that it snaps to the cursor position and not the "
"bounding box of the layer, selection or whatever you are trying to move)"
msgstr ""

#: ../../user_manual/snapping.rst:96
msgid "The Transform tool"
msgstr ""

#: ../../user_manual/snapping.rst:97
msgid "Rectangle select"
msgstr ""

#: ../../user_manual/snapping.rst:98
msgid "Elliptical select"
msgstr ""

#: ../../user_manual/snapping.rst:99
msgid "Polygonal select"
msgstr ""

#: ../../user_manual/snapping.rst:100
msgid "Path select"
msgstr ""

#: ../../user_manual/snapping.rst:101
msgid "Guides themselves can be snapped to grids and vectors"
msgstr ""

#: ../../user_manual/snapping.rst:103
msgid ""
"Snapping doesn’t have a sensitivity yet, and by default is set to 10 screen "
"pixels."
msgstr ""
