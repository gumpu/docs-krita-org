# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:00+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en projectionanimation projection Krita\n"
"X-POFile-SpellExtra: projectionimage image menuselection kbd gif\n"
"X-POFile-SpellExtra: categoryprojection images\n"
"X-POFile-IgnoreConsistency: Oblique\n"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_01.svg"
msgstr ".. image:: images/category_projection/projection-cube_01.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_02.svg"
msgstr ".. image:: images/category_projection/projection-cube_02.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_03.svg"
msgstr ".. image:: images/category_projection/projection-cube_03.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_04.svg"
msgstr ".. image:: images/category_projection/projection-cube_04.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_05.svg"
msgstr ".. image:: images/category_projection/projection-cube_05.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_06.svg"
msgstr ".. image:: images/category_projection/projection-cube_06.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_01.png"
msgstr ".. image:: images/category_projection/projection_image_01.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_02.png"
msgstr ".. image:: images/category_projection/projection_image_02.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_03.png"
msgstr ".. image:: images/category_projection/projection_image_03.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_04.png"
msgstr ".. image:: images/category_projection/projection_image_04.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_05.png"
msgstr ".. image:: images/category_projection/projection_image_05.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_06.png"
msgstr ".. image:: images/category_projection/projection_image_06.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_07.png"
msgstr ".. image:: images/category_projection/projection_image_07.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_08.png"
msgstr ".. image:: images/category_projection/projection_image_08.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_09.png"
msgstr ".. image:: images/category_projection/projection_image_09.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_10.png"
msgstr ".. image:: images/category_projection/projection_image_10.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_11.png"
msgstr ".. image:: images/category_projection/projection_image_11.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_12.png"
msgstr ".. image:: images/category_projection/projection_image_12.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_13.png"
msgstr ".. image:: images/category_projection/projection_image_13.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_14.png"
msgstr ".. image:: images/category_projection/projection_image_14.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_animation_01.gif"
msgstr ".. image:: images/category_projection/projection_animation_01.gif"

#: ../../general_concepts/projection/orthographic_oblique.rst:1
msgid "Orthographics and oblique projection."
msgstr "Ortografia e projecção oblíqua."

#: ../../general_concepts/projection/orthographic_oblique.rst:10
msgid "So let's start with the basics..."
msgstr "Bem, vamos começar pelo básico..."

#: ../../general_concepts/projection/orthographic_oblique.rst:12
#: ../../general_concepts/projection/orthographic_oblique.rst:16
msgid "Orthographic"
msgstr "Ortográfica"

#: ../../general_concepts/projection/orthographic_oblique.rst:12
msgid "Projection"
msgstr "Projecção"

#: ../../general_concepts/projection/orthographic_oblique.rst:18
msgid ""
"Despite the fancy name, you probably know what orthographic is. It is a "
"schematic representation of an object, draw undeformed. Like the following "
"example:"
msgstr ""
"Apesar do nome bonito, provavelmente já sabe o que é a ortográfica. É uma "
"representação em esquema de um objecto, desenhada sem deformações. Como o "
"seguinte exemplo:"

#: ../../general_concepts/projection/orthographic_oblique.rst:23
msgid ""
"This is a rectangle. We have a front, top and side view. Put into "
"perspective it should look somewhat like this:"
msgstr ""
"Isto é um rectângulo. Temos a vista frontal, de topo e lateral. Colocando em "
"perspectiva, deverá ficar algo semelhante a isto:"

#: ../../general_concepts/projection/orthographic_oblique.rst:28
msgid ""
"While orthographic representations are kinda boring, they're also a good "
"basis to start with when you find yourself in trouble with a pose. But we'll "
"get to that in a bit."
msgstr ""
"Embora as representações ortográficas sejam um pouco aborrecidas, são também "
"uma boa base para começar quando se deparar com problemas numa pose. Mas "
"iremos lá chegar daqui a pouco."

#: ../../general_concepts/projection/orthographic_oblique.rst:33
msgid "Oblique"
msgstr "Oblíquo"

#: ../../general_concepts/projection/orthographic_oblique.rst:35
msgid ""
"So, if we can say that the front view is the viewer looking at the front, "
"and the side view is the viewer directly looking at the side. (The "
"perpendicular line being the view plane it is projected on)"
msgstr ""
"Por isso, se pudermos afirmar que a vista frontal é o observador a olhar "
"para a parte da frente, e a vista lateral é o observador a olhar "
"directamente para um dos lados. (A linha perpendicular é o plano de "
"observação sobre o qual está projectado)"

#: ../../general_concepts/projection/orthographic_oblique.rst:40
msgid "Then we can get a half-way view from looking from an angle, no?"
msgstr ""
"Assim podemos obter uma vista a meio-caminho para observar a partir de um "
"dado ângulo, não?"

#: ../../general_concepts/projection/orthographic_oblique.rst:45
msgid "If we do that for a lot of different sides…"
msgstr "Se o fizermos para uma variedade de lados diferentes…"

#: ../../general_concepts/projection/orthographic_oblique.rst:50
msgid "And we line up the sides we get a…"
msgstr "E alinharmos os lados, iremos obter um…"

#: ../../general_concepts/projection/orthographic_oblique.rst:55
msgid ""
"But cubes are boring. I am suspecting that projection is so ignored because "
"no tutorial applies it to an object where you actually might NEED "
"projection. Like a face."
msgstr ""
"Mas os cubos são aborrecidos. Suspeito que essa projecção é assim ignorada, "
"porque nenhum tutorial a aplica a um objecto onde realmente PRECISA da "
"projecção. Como uma cara."

#: ../../general_concepts/projection/orthographic_oblique.rst:57
msgid "First, let's prepare our front and side views:"
msgstr "Primeiro, vamos preparar as nossas vistas frontal e laterais:"

#: ../../general_concepts/projection/orthographic_oblique.rst:62
msgid ""
"I always start with the side, and then extrapolate the front view from it. "
"Because you are using Krita, set up two parallel rulers, one vertical and "
"the other horizontal. To snap them perfectly, drag one of the nodes after "
"you have made the ruler, and press the :kbd:`Shift` key to snap it "
"horizontal or vertical. In 3.0, you can also snap them to the image borders "
"if you have :menuselection:`Snap Image Bounds` active via the :kbd:`Shift + "
"S` shortcut."
msgstr ""
"O autor começa sempre pelo lado, extrapolando depois a vista frontal a "
"partir dela. Como está a usar o Krita, configure duas réguas paralelas, uma "
"vertical e a outra horizontal. Para as ajustar perfeitamente, desenhe um dos "
"nós após ter criado a régua e depois carregue em :kbd:`Shift` para a ajustar "
"na horizontal ou na vertical. No 3.0, também poderá ajustá-las aos contornos "
"das imagens, caso tenha a opção :menuselection:`Ajustar aos Contornos das "
"Imagens` com a opção :kbd:`Shift + S`."

#: ../../general_concepts/projection/orthographic_oblique.rst:64
msgid ""
"Then, by moving the mirror to the left, you can design a front view from the "
"side view, while the parallel preview line helps you with aligning the eyes "
"(which in the above screenshot are too low)."
msgstr ""
"Depois, ao mover o espelho para a esquerda, poderá desenhar uma vista "
"frontal a partir da vista lateral, enquanto a linha de antevisão paralela o "
"ajuda a alinhar os olhos (que na imagem acima estão muito baixos)."

#: ../../general_concepts/projection/orthographic_oblique.rst:66
msgid "Eventually, you should have something like this:"
msgstr "Eventualmente, deverá ter algo do género:"

#: ../../general_concepts/projection/orthographic_oblique.rst:71
msgid "And of course, let us not forget the top, it's pretty important:"
msgstr "E, obviamente, não nos esqueçamos do topo, que é bastante importante:"

#: ../../general_concepts/projection/orthographic_oblique.rst:78
msgid ""
"When you are using Krita, you can just use transform masks to rotate the "
"side view for drawing the top view."
msgstr ""
"Quando estiver a usar o Krita, poderá simplesmente usar máscaras de "
"transformação para rodar a vista lateral para desenhar a vista de topo."

#: ../../general_concepts/projection/orthographic_oblique.rst:80
msgid ""
"The top view works as a method for debugging your orthos as well. If we take "
"the red line to figure out the orthographics from, we see that our eyes are "
"obviously too inset. Let's move them a bit more forward, to around the nose."
msgstr ""
"A vista de topo funciona como um método para depuração das suas ortografias. "
"Se usarmos a linha vermelha para descobrir a ortografia, veremos que os "
"nossos olhos estão demasiado para dentro. Vamos avançá-los um pouco mais, "
"para perto do nariz."

#: ../../general_concepts/projection/orthographic_oblique.rst:85
msgid ""
"If you want to do precision position moving in the tool options docker, just "
"select 'position' and the input box for the X. Pressing down then moves the "
"transformed selection left. With Krita 3.0 you can just use the move tool "
"for this and the arrow keys. Using transform here can be more convenient if "
"you also have to squash and stretch an eye."
msgstr ""
"Se quiser movimentos com precisão na área de opções da ferramenta, basta "
"seleccionar 'posição'e  o campo de texto do X. Se carregar para baixo, move "
"a selecção transformada para a esquerda. Com o Krita 3.0, poderá "
"simplesmente usar a ferramenta de movimento para isto, em conjunto com as "
"teclas de cursores. O uso da transformação aqui pode ser mais conveniente se "
"também tiver de esmagar ou esticar um olho."

#: ../../general_concepts/projection/orthographic_oblique.rst:90
msgid "We fix the top view now. Much better."
msgstr "Corrigimos agora a vista de topo. Muito melhor."

#: ../../general_concepts/projection/orthographic_oblique.rst:92
msgid ""
"For faces, the multiple slices are actually pretty important. So important "
"even, that I have decided we should have these slices on separate layers. "
"Thankfully, I chose to color them, so all we need to do is go to :"
"menuselection:`Layer --> Split Layer` ."
msgstr ""
"Para as caras, as várias fatias são de facto bastante importantes. Tão "
"importantes que decidimos que essas fatias devem estar em camadas separadas. "
"Felizmente, optámos por colori-las, pelo que tudo o que é necessário fazer é "
"ir a :menuselection:`Camada --> Dividir a Camada` ."

#: ../../general_concepts/projection/orthographic_oblique.rst:98
msgid ""
"This'll give you a few awkwardly named layers… rename them by selecting all "
"and mass changing the name in the properties editor:"
msgstr ""
"Isto irá gerar algumas camadas com nomes estranhos… mude o nome delas, "
"seleccionando-as todas e mudando o nome em lote no editor de propriedades:"

#: ../../general_concepts/projection/orthographic_oblique.rst:103
msgid "So, after some cleanup, we should have the following:"
msgstr "Assim, depois de alguma limpeza, deveremos ter o seguinte:"

#: ../../general_concepts/projection/orthographic_oblique.rst:108
msgid "Okay, now we're gonna use animation for the next bit."
msgstr "Ok, agora iremos usar a animação para o próximo pedaço."

#: ../../general_concepts/projection/orthographic_oblique.rst:110
msgid "Set it up as follows:"
msgstr "Configure-o como se segue:"

#: ../../general_concepts/projection/orthographic_oblique.rst:115
msgid ""
"Both front view and side view are set up as 'visible in timeline' so we can "
"always see them."
msgstr ""
"Tanto a vista frontal como a lateral estão configuradas como 'visíveis na "
"linha temporal', pelo que podemos vê-las sempre."

#: ../../general_concepts/projection/orthographic_oblique.rst:116
msgid ""
"Front view has its visible frame on frame 0 and an empty frame on frame 23."
msgstr ""
"A vista frontal tem a sua imagem visível na imagem 0 e uma imagem vazia na "
"imagem 23."

#: ../../general_concepts/projection/orthographic_oblique.rst:117
msgid ""
"Side view has its visible frame on frame 23 and an empty view on frame 0."
msgstr ""
"A vista lateral tem a sua imagem visível na imagem 23 e uma imagem vazia na "
"imagem 0."

#: ../../general_concepts/projection/orthographic_oblique.rst:118
msgid "The end of the animation is set to 23."
msgstr "O fim da animação está configurado como 23."

#: ../../general_concepts/projection/orthographic_oblique.rst:123
msgid ""
"Krita can't animate a transformation on multiple layers on multiple frames "
"yet, so let's just only transform the top layer. Add a semi-transparent "
"layer where we draw the guidelines."
msgstr ""
"O Krita ainda não consegue animar uma transformação em várias camadas com "
"várias imagens, pelo que iremos apenas transformar a camada de topo. "
"Adicione uma camada semi-transparente onde são desenhadas as linhas-guias."

#: ../../general_concepts/projection/orthographic_oblique.rst:125
msgid ""
"Now, select frame 11 (halfway), add new frames from front view, side view "
"and the guidelines. And turn on the onion skin by toggling the lamp symbols. "
"We copy the frame for the top view and use the transform tool to rotate it "
"45°."
msgstr ""
"Agora, seleccione a imagem 11 (a meio-caminho), adicione novas imagens da "
"vista frontal, da vista lateral e das linhas-guias. Depois, active a 'pele "
"de cebola', ligando todos os símbolos de lâmpadas. Iremos copiar a imagem "
"para a vista de topo e iremos usar a ferramenta de transformação para a "
"rodar em 45°."

#: ../../general_concepts/projection/orthographic_oblique.rst:130
msgid "So, we draw our vertical guides again and determine a in-between..."
msgstr ""
"Assim, desenhamos as nossas guias verticais de novo e determinar um ponto "
"intermédio..."

#: ../../general_concepts/projection/orthographic_oblique.rst:135
msgid ""
"This is about how far you can get with only the main slice, so rotate the "
"rest as well."
msgstr ""
"Isto é o máximo que consegue obter apenas com a fatia principal, portanto "
"rode o resto também."

#: ../../general_concepts/projection/orthographic_oblique.rst:140
msgid "And just like with the cube, we do this for all slices…"
msgstr "E tal como acontece com o cubo, fazemos isso para todas as fatias…"

#: ../../general_concepts/projection/orthographic_oblique.rst:145
msgid ""
"Eventually, if you have the top slices rotate every frame with 15°, you "
"should be able to make a turn table, like this:"
msgstr ""
"Eventualmente, se tivermos as fatias de topo a rodar em 15° por cada imagem, "
"deverá conseguir fazer uma rotação completa, como a seguinte:"

#: ../../general_concepts/projection/orthographic_oblique.rst:150
msgid ""
"Because our boy here is fully symmetrical, you can just animate one side and "
"flip the frames for the other half."
msgstr ""
"Como o nosso rapaz aqui é completamente simétrico, poderá apenas animar um "
"lado e inverter as imagens para a outra metade."

#: ../../general_concepts/projection/orthographic_oblique.rst:152
msgid ""
"While it is not necessary to follow all the steps in the theory section to "
"understand the tutorial, I do recommend making a turn table sometime. It "
"teaches you a lot about drawing 3/4th faces."
msgstr ""
"Embora não seja necessário seguir todos os passos da secção teórica para "
"compreender o tutorial, recomenda-se que crie uma imagem rotativa em alguma "
"altura. Ensina-lhe bastante sobre o desenho de caras em 3/4."

#: ../../general_concepts/projection/orthographic_oblique.rst:154
msgid "How about… we introduce the top view into the drawing itself?"
msgstr "E agora… se introduzíssemos a vista de topo no desenho em si?"
