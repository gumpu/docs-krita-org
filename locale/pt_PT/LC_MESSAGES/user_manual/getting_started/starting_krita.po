# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:07+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en krita guilabel image menuselection kbd images alt\n"
"X-POFile-SpellExtra: basicconcepts icons Starting freehandbrushtool\n"
"X-POFile-SpellExtra: toolfreehandbrush KritaBrushPresetDocker ref\n"
"X-POFile-SpellExtra: savingfortheweb colormanagementsettings Krita kra\n"
"X-POFile-SpellExtra: navigation Kritanewfile dockers createnewdocument\n"
"X-POFile-SpellExtra: generalconceptcolor\n"

#: ../../<rst_epilog>:22
msgid ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: toolfreehandbrush"
msgstr ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: ferramenta de pincel à mão"

#: ../../user_manual/getting_started/starting_krita.rst:None
msgid ".. image:: images/Starting-krita.png"
msgstr ".. image:: images/Starting-krita.png"

#: ../../user_manual/getting_started/starting_krita.rst:1
msgid ""
"A simple guide to the first basic steps of using Krita: creating and saving "
"an image."
msgstr ""
"Um guia simples para os primeiros passos básicos de utilização do Krita: a "
"criação e gravação de uma imagem."

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "Getting started"
msgstr "Introdução"

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "Save"
msgstr "Gravar"

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "Load"
msgstr "Carregar"

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "New"
msgstr "Novo"

#: ../../user_manual/getting_started/starting_krita.rst:22
msgid "Starting Krita"
msgstr "Iniciar o Krita"

#: ../../user_manual/getting_started/starting_krita.rst:24
msgid ""
"When you start Krita for the first time there will be no canvas or new "
"document open by default. You will be greeted by a :ref:`welcome screen "
"<welcome_screen>`, which will have option to create a new file or open "
"existing document. To create a new canvas you have to create a new document "
"from the :guilabel:`File` menu or by clicking on :guilabel:`New File`  under "
"start section of the welcome screen. This will open the new file dialog box. "
"If you want to open an existing image, either use :menuselection:`File --> "
"Open` or drag the image from your computer into Krita's window."
msgstr ""
"Quando iniciar o Krita pela primeira vez, não existirá nenhuma área de "
"desenho ou novo documento aberto por omissão. Será recebido com um :ref:"
"`ecrã de boas-vindas <welcome_screen>`, o qual terá a opção para criar um "
"novo ficheiro ou abrir um existente. Para criar uma nova área de desenho, "
"terá de criar um novo documento a partir do menu :guilabel:`Ficheiro`. Se "
"quiser abrir uma imagem existe, pode então usar o :menuselection:`Ficheiro --"
"> Abrir` ou arrastar a imagem do seu computador para a janela do Krita."

#: ../../user_manual/getting_started/starting_krita.rst:37
msgid "Creating a New Document"
msgstr "Criar um Novo Documento"

#: ../../user_manual/getting_started/starting_krita.rst:39
msgid "A new document can be created as follows."
msgstr "Pode ser criado um novo documento da seguinte forma."

#: ../../user_manual/getting_started/starting_krita.rst:41
msgid "Click on :guilabel:`File` from the application menu at the top."
msgstr "Carregue em :guilabel:`Ficheiro` no menu de aplicações no topo."

#: ../../user_manual/getting_started/starting_krita.rst:42
msgid ""
"Then click on :guilabel:`New`. Or you can do this by pressing the :kbd:`Ctrl "
"+ N` shortcut."
msgstr ""
"Depois carregue em :guilabel:`Novo`. Ou então poderá fazer isto se carregar "
"em :kbd:`Ctrl + N`."

#: ../../user_manual/getting_started/starting_krita.rst:43
msgid "Now you will get a New Document dialog box as shown below:"
msgstr "Agora irá obter uma janela de Novo Documento, como aparece abaixo:"

#: ../../user_manual/getting_started/starting_krita.rst:46
msgid ".. image:: images/Krita_newfile.png"
msgstr ".. image:: images/Krita_newfile.png"

#: ../../user_manual/getting_started/starting_krita.rst:47
msgid ""
"Click on the :guilabel:`Custom Document` section and in the :guilabel:"
"`Dimensions` tab choose A4 (300ppi) or any size that you prefer from the :"
"guilabel:`predefined` drop down To know more about the other sections such "
"as create document from clipboard and templates see :ref:"
"`create_new_document`."
msgstr ""
"Carregue na secção :guilabel:`Documento Personalizado` e na página :guilabel:"
"`Dimensões` escolha A4 (300ppp) ou qualquer tamanho que preferir na lista :"
"guilabel:`predefinidos`. Para saber mais sobre as outras secções, como a "
"criação de um documento a partir da área de transferência ou dos modelos, "
"veja :ref:`create_new_document`."

#: ../../user_manual/getting_started/starting_krita.rst:52
msgid ""
"Make sure that the color profile is RGB and depth is set to 8-bit integer/"
"channel in the color section. For advanced information about the color and "
"color management refer to :ref:`general_concept_color`."
msgstr ""
"Certifique-se que o perfil de cores é o RGB e a profundidade está "
"configurada como um valor de 8 bits inteiros/canal na secção de cores. Para "
"informações mais avançadas sobre cores e gestão de cores, consulte a página :"
"ref:`general_concept_color`."

#: ../../user_manual/getting_started/starting_krita.rst:56
msgid "How to use brushes"
msgstr "Como usar os pincéis"

#: ../../user_manual/getting_started/starting_krita.rst:58
msgid ""
"Now, on the blank white canvas, just left click with your mouse or draw with "
"the pen on a graphic tablet. If everything's correct, you should be able to "
"draw on the canvas! The brush tool should be selected by default when you "
"start Krita, but if for some reason it is not, you can click on this |"
"toolfreehandbrush| icon from the toolbox and activate the brush tool."
msgstr ""
"Agora, na área de desenho em branco basta carregar na componente da área de "
"desenho. Se tudo estiver correcto, deverá ser capaz de desenhar na mesma! A "
"ferramenta do pincel deverá estar escolhida por omissão quando iniciar o "
"Krita mas, se por algum razão não estiver, poderá carregar neste ícone |"
"toolfreehandbrush| da barra de ferramentas e activar a ferramenta do pincel."

#: ../../user_manual/getting_started/starting_krita.rst:64
msgid ""
"Of course, you'd want to use different brushes. On your right, there's a "
"docker named Brush Presets (or on top, press the :kbd:`F6` key to find this "
"one) with all these cute squares with pens and crayons."
msgstr ""
"Obviamente, irá querer ver pincéis diferentes. À sua direita, existe uma "
"área chamada Predefinições de Pincéis (ou no topo; carregue em :kbd:`F6` "
"para a descobrir) com todos estes quadrados bonitos com canetas e pincéis."

#: ../../user_manual/getting_started/starting_krita.rst:68
msgid ""
"If you want to tweak the presets, check the Brush Editor in the toolbar. You "
"can also access the Brush Editor with the :kbd:`F5` key."
msgstr ""
"Se quiser afinar as predefinições, veja o Editor de Pincéis na barra de "
"ferramentas. Poderá também aceder ao Editor de Pincéis com o :kbd:`F5`."

#: ../../user_manual/getting_started/starting_krita.rst:72
msgid ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"
msgstr ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"

#: ../../user_manual/getting_started/starting_krita.rst:73
msgid ""
"Tick any of the squares to choose a brush, and then draw on the canvas. To "
"change color, click the triangle in the Advanced Color Selector docker."
msgstr ""
"Assinale qualquer um dos quadrados para escolher um pincel, desenhando "
"depois na área de desenho. Para mudar a cor, carregue no triângulo na área "
"do Selector de Cores Avançadas."

#: ../../user_manual/getting_started/starting_krita.rst:77
msgid "Erasing"
msgstr "Apagar"

#: ../../user_manual/getting_started/starting_krita.rst:79
msgid ""
"There are brush presets for erasing, but it is often faster to use the "
"eraser toggle. By toggling the :kbd:`E` key, your current brush switches "
"between erasing and painting. This erasing method works with most of the "
"tools. You can erase using the line tool, rectangle tool, and even the "
"gradient tool."
msgstr ""
"Existem predefinições de pincéis para apagar ou limpar, mas normalmente é "
"mais rápido usar a borracha. Ao activar a tecla :kbd:`E`, o seu pincel "
"actual alterna entre a pintura e a limpeza. Este método de limpeza funciona "
"com a maior parte das ferramentas. Poderá apagar com a ferramenta de linhas, "
"rectângulo ou mesmo com os gradientes."

#: ../../user_manual/getting_started/starting_krita.rst:85
msgid "Saving and opening files"
msgstr "Gravar e abrir os ficheiros"

#: ../../user_manual/getting_started/starting_krita.rst:87
msgid ""
"Now, once you have figured out how to draw something in Krita, you may want "
"to save it. The save option is in the same place as it is in all other "
"computer programs: the top-menu of :guilabel:`File`, and then :guilabel:"
"`Save`. Select the folder you want to have your drawing, and select the file "
"format you want to use ('.kra' is Krita's default format, and will save "
"everything). And then hit :guilabel:`Save`. Some older versions of Krita "
"have a bug and require you to manually type the extension."
msgstr ""
"Agora que descobriu como desenhar algo no Krita, poderá querer gravá-lo. A "
"opção de gravação está no mesmo local que nos outros programas para "
"computadores, no menu de topo :guilabel:`Ficheiro` e depois em :guilabel:"
"`Gravar`. Seleccione a pasta onde deseja gravar o seu desenho e seleccione o "
"formato de ficheiros que deseja usar (o '.kra' é o formato predefinido do "
"Krita's e será capaz de gravar tudo). Depois disto, carregue em :guilabel:"
"`Gravar`. Algumas versões mais antigas do Krita têm um erro onde o obrigam a "
"escrever manualmente a extensão."

#: ../../user_manual/getting_started/starting_krita.rst:95
msgid ""
"If you want to show off your image on the internet, check out the :ref:"
"`saving_for_the_web` tutorial."
msgstr ""
"Se quiser apresentar a sua imagem na Internet, veja o tutorial sobre :ref:"
"`saving_for_the_web`."

#: ../../user_manual/getting_started/starting_krita.rst:98
msgid ""
"Check out :ref:`navigation` for further basic information, :ref:"
"`basic_concepts` for an introduction as Krita as a medium, or just go out "
"and explore Krita!"
msgstr ""
"Consulte a :ref:`navigation` para obter mais algumas informações básicas, o :"
"ref:`basic_concepts` para obter uma introdução média ao Krita ou então "
"prossiga e explore o Krita!"
