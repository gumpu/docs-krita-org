# Translation of docs_krita_org_reference_manual___brushes___brush_settings___masked_brush.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-01 03:12+0200\n"
"PO-Revision-Date: 2019-06-15 19:51+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:0
msgid ".. image:: images/brushes/Masking-brush2.jpg"
msgstr ".. image:: images/brushes/Masking-brush2.jpg"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:None
msgid ".. image:: images/brushes/Masking-brush1.jpg"
msgstr ".. image:: images/brushes/Masking-brush1.jpg"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:1
msgid ""
"How to use the masked brush functionality in Krita. This functionality is "
"not unlike the dual brush option from photoshop."
msgstr ""
"Com s'utilitza la funcionalitat del pinzell emmascarat al Krita. Aquesta "
"funcionalitat no és diferent de l'opció pinzell doble del Photoshop."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:13
#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:18
msgid "Masked Brush"
msgstr "Pinzell emmascarat"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:13
msgid "Dual Brush"
msgstr "Pinzell doble"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:13
msgid "Stacked Brush"
msgstr "Pinzell apilat"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:23
msgid ""
"Masked brush is new feature that is only available in the :ref:"
"`pixel_brush_engine`. They are additional settings you will see in the brush "
"editor. Masked brushes allow you to combine two brush tips in one stroke. "
"One brush tip will be a mask for your primary brush tip. A masked brush is a "
"good alternative to texture for creating expressive and textured brushes."
msgstr ""
"El pinzell emmascarat és una característica nova que només està disponible "
"al :ref:`pixel_brush_engine`. Són ajustaments addicionals que veureu a "
"l'editor de pinzells. Els pinzells emmascarats permeten combinar dues puntes "
"de pinzell en un sol traç. Una punta del pinzell serà una màscara per a la "
"punta del pinzell primari. Un pinzell emmascarat és una bona alternativa a "
"la textura per a crear pinzells expressius i amb textura."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:29
msgid ""
"Due to technological constraints, the masked brush only works in the wash "
"painting mode. However, do remember that flow works as opacity does in the "
"build-up painting mode."
msgstr ""
"A causa de limitacions tecnològiques, el pinzell emmascarat només funciona "
"en el mode de pintura Rentatge. No obstant això, recordeu que el flux "
"funciona com ho fa l'opacitat en el mode de pintura Construeix."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:32
msgid ""
"Like with normal brush tip you can choose any brush tip and change it size, "
"spacing, and rotation. Masking brush size is relative to main brush size. "
"This means when you change your brush size masking tip will be changed to "
"keep the ratio."
msgstr ""
"Igual que amb la punta de pinzell normal, podreu triar qualsevol punta de "
"pinzell i canviar la seva mida, espaiat i gir. La mida del pinzell "
"d'emmascarament és relativa a la mida del pinzell principal. Això vol dir "
"que quan canvieu la mida del pinzell, es canviarà la punta de "
"l'emmascarament per a mantenir la proporció."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:35
msgid ":ref:`Blending mode (drop-down inside Brush tip)<blending_modes>`:"
msgstr ""
":ref:`Mode de barreja (desplegable dins la Punta del pinzell) "
"<blending_modes>`:"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:35
msgid "Blending modes changes how tips are combined."
msgstr "Els modes de barreja canvien la forma en què es combinen les puntes."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:38
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:40
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:41
msgid "The size sensor option of the second tip."
msgstr "L'opció del sensor per a la mida de la segona punta."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:42
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:43
msgid ""
"The opacity and flow of the second tip. This is mapped to a sensor by "
"default. Flow can be quite aggressive on subtract mode, so it might be an "
"idea to turn it off there."
msgstr ""
"L'opacitat i el flux de la segona punta. Això s'assigna a un sensor "
"predeterminat. El flux pot ser força agressiu en el mode sostreu, de manera "
"que podria ser una idea desactivar-lo."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:44
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:45
msgid "This affects the brush ratio on a given brush."
msgstr "Això afectarà la relació del pinzell sobre un pinzell indicat."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:46
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:47
msgid "The Mirror option of the second tip."
msgstr "L'opció del mirall per a la mida de la segona punta."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:48
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:49
msgid "The rotation option of the second tip. Best set to \"fuzzy dab\"."
msgstr ""
"L'opció del gir per a la mida de la segona punta. Millor si s'estableix a "
"«Toc difús»."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:51
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:51
msgid ""
"The scatter option. The default is quite high, so don't forget to turn it "
"lower."
msgstr ""
"L'opció de dispersió. De manera predeterminada és molt alta, així que no "
"oblideu reduir-la."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:53
msgid "Difference from :ref:`option_texture`:"
msgstr "Diferència de :ref:`option_texture`:"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:55
msgid "You don’t need seamless texture to make cool looking brush"
msgstr ""
"No necessitareu una textura perfecta per a crear un pinzell d'aspecte fresc."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:56
msgid "Stroke generates on the fly, it always different"
msgstr "El traç es generarà sobre la marxa, sempre serà diferent."

# skip-rule: barb-igual
#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:57
msgid "Brush strokes looks same on any brush size"
msgstr "Les pinzellades es veuran igual en qualsevol mida del pinzell."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:58
msgid ""
"Easier to fill some areas with solid color but harder to make it hard "
"textured"
msgstr ""
"Més fàcil per emplenar algunes àrees amb un color sòlid, però més difícil "
"per a fer que tingui una textura dura."
