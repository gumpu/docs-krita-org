# Translation of docs_krita_org_reference_manual___main_menu___file_menu.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 21:13+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/main_menu/file_menu.rst:1
msgid "The file menu in Krita."
msgstr "El menú Fitxer en el Krita."

#: ../../reference_manual/main_menu/file_menu.rst:12
#: ../../reference_manual/main_menu/file_menu.rst:23
msgid "Open"
msgstr "Obre"

#: ../../reference_manual/main_menu/file_menu.rst:12
#: ../../reference_manual/main_menu/file_menu.rst:29
msgid "Save"
msgstr "Desa"

#: ../../reference_manual/main_menu/file_menu.rst:12
#: ../../reference_manual/main_menu/file_menu.rst:38
msgid "Export"
msgstr "Exporta"

#: ../../reference_manual/main_menu/file_menu.rst:12
#: ../../reference_manual/main_menu/file_menu.rst:62
msgid "Close"
msgstr "Tanca"

#: ../../reference_manual/main_menu/file_menu.rst:12
msgid "New File"
msgstr "Fitxer nou"

#: ../../reference_manual/main_menu/file_menu.rst:12
msgid "Import"
msgstr "Importa"

#: ../../reference_manual/main_menu/file_menu.rst:12
msgid "Template"
msgstr "Plantilla"

#: ../../reference_manual/main_menu/file_menu.rst:17
msgid "File Menu"
msgstr "El menú Fitxer"

#: ../../reference_manual/main_menu/file_menu.rst:20
msgid "New"
msgstr "Nova"

#: ../../reference_manual/main_menu/file_menu.rst:22
msgid "Make a new file."
msgstr "Crea un fitxer nou."

#: ../../reference_manual/main_menu/file_menu.rst:25
msgid "Open a previously created file."
msgstr "Obre un fitxer creat anteriorment."

#: ../../reference_manual/main_menu/file_menu.rst:26
msgid "Open Recent"
msgstr "Obre recent"

#: ../../reference_manual/main_menu/file_menu.rst:28
msgid "Open the recently opened document."
msgstr "Obre el document obert recentment."

#: ../../reference_manual/main_menu/file_menu.rst:31
msgid ""
"File formats that Krita can save to. These formats can later be opened back "
"up in Krita."
msgstr ""
"Formats de fitxer que el Krita pot desar. Aquests formats es podran obrir "
"més tard en el Krita."

#: ../../reference_manual/main_menu/file_menu.rst:32
msgid "Save As"
msgstr "Desa com a"

#: ../../reference_manual/main_menu/file_menu.rst:34
msgid "Save as a new file."
msgstr "Desa com un fitxer nou."

#: ../../reference_manual/main_menu/file_menu.rst:35
msgid "Open Existing Document As New document"
msgstr "Obre un document existent com a un document nou"

#: ../../reference_manual/main_menu/file_menu.rst:37
msgid "Similar to import in other programs."
msgstr "Similar a importa en altres programes."

#: ../../reference_manual/main_menu/file_menu.rst:40
msgid ""
"Additional formats that can be saved. Some of these formats may not be later "
"imported or opened by Krita."
msgstr ""
"Formats addicionals que es poden desar. Alguns d'aquests formats no podran "
"ser importats o oberts posteriorment pel Krita."

#: ../../reference_manual/main_menu/file_menu.rst:41
msgid "Import Animation Frames"
msgstr "Importa fotogrames d'animació"

#: ../../reference_manual/main_menu/file_menu.rst:43
msgid "Import frames for animation."
msgstr "Importa fotogrames per a animació."

#: ../../reference_manual/main_menu/file_menu.rst:44
msgid "Render Animation"
msgstr "Renderitza l'animació"

#: ../../reference_manual/main_menu/file_menu.rst:46
msgid ""
"Render an animation with FFmpeg. This is explained on the :ref:"
"`render_animation` page."
msgstr ""
"Renderitza una animació amb FFmpeg. Això s'explica a la pàgina :ref:"
"`render_animation`."

#: ../../reference_manual/main_menu/file_menu.rst:47
msgid "Save incremental version"
msgstr "Desa la versió incremental"

#: ../../reference_manual/main_menu/file_menu.rst:49
msgid "Save as a new version of the same file with a number attached."
msgstr "Desa com a nova versió del mateix fitxer amb un número adjuntat."

#: ../../reference_manual/main_menu/file_menu.rst:50
msgid "Save incremental Backup"
msgstr "Desa la còpia de seguretat incremental"

#: ../../reference_manual/main_menu/file_menu.rst:52
msgid ""
"Copies and renames the last saved version of your file to a back-up file and "
"saves your document under the original name."
msgstr ""
"Copia i reanomena l'última versió desada del vostre fitxer en un fitxer de "
"còpia de seguretat i desa el vostre document amb el nom original."

#: ../../reference_manual/main_menu/file_menu.rst:53
msgid "Create Template from image"
msgstr "Crea una plantilla des de la imatge"

#: ../../reference_manual/main_menu/file_menu.rst:55
msgid ""
"The \\*.kra file will be saved into the template folder for future use. All "
"your layers and guides will be saved along!"
msgstr ""
"El fitxer \\*.kra es desarà a la carpeta de plantilles per al seu ús futur. "
"Es desaran totes les vostres capes i guies!"

#: ../../reference_manual/main_menu/file_menu.rst:56
msgid "Create Copy From Current Image"
msgstr "Crea una còpia des de la imatge actual"

#: ../../reference_manual/main_menu/file_menu.rst:58
msgid ""
"Makes a new document from the current image, so you can easily reiterate on "
"a single image. Useful for areas where the template system is too powerful."
msgstr ""
"Crea un document nou a partir de la imatge actual, de manera que podreu "
"reiterar amb facilitat sobre una sola imatge. Útil per a les àrees on el "
"sistema de plantilles és massa poderós."

#: ../../reference_manual/main_menu/file_menu.rst:59
msgid "Document Information"
msgstr "Informació del document"

#: ../../reference_manual/main_menu/file_menu.rst:61
msgid ""
"Look at the document information. Contains all sorts of interesting "
"information about image, such as technical information or metadata."
msgstr ""
"Mira la informació del document. Conté tot tipus d'informació interessant "
"sobre la imatge, com la informació tècnica o les metadades."

#: ../../reference_manual/main_menu/file_menu.rst:64
msgid "Close the view or document."
msgstr "Tanca la vista o el document."

#: ../../reference_manual/main_menu/file_menu.rst:65
msgid "Close All"
msgstr "Tanca-ho tot"

#: ../../reference_manual/main_menu/file_menu.rst:67
msgid "Close all views and documents."
msgstr "Tanca totes les vistes i documents."

#: ../../reference_manual/main_menu/file_menu.rst:68
msgid "Quit"
msgstr "Surt"

#: ../../reference_manual/main_menu/file_menu.rst:70
msgid "Close Krita."
msgstr "Tanca el Krita."
