# Translation of docs_krita_org_reference_manual___main_menu___layers_menu.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 21:14+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Layerstyle (2.9.5+)"
msgstr "Estil de la capa (2.9.5+)"

#: ../../reference_manual/main_menu/layers_menu.rst:1
msgid "The layers menu in Krita."
msgstr "El menú Capes en el Krita."

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:75
msgid "Convert"
msgstr "Converteix"

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:115
msgid "Transform"
msgstr "Transforma"

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:134
msgid "Histogram"
msgstr "Histograma"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Layers"
msgstr "Capes"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Cut Layer"
msgstr "Retalla la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Copy Layer"
msgstr "Copia una capa"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Paste Layer"
msgstr "Enganxa la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Import"
msgstr "Importa"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Export"
msgstr "Exporta"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Metadata"
msgstr "Metadades"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Flatten"
msgstr "Aplana"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Layer Style"
msgstr "Estil de la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:16
msgid "Layers Menu"
msgstr "El menú Capes"

#: ../../reference_manual/main_menu/layers_menu.rst:18
msgid ""
"These are the topmenu options are related to Layer Management, check out :"
"ref:`that page <layers_and_masks>` first, if you haven't."
msgstr ""
"Aquestes són les opcions del menú principal relacionades amb la gestió de "
"les capes, reviseu :ref:`aquesta pàgina <layers_and_masks>` primer, si no ho "
"heu fet."

#: ../../reference_manual/main_menu/layers_menu.rst:20
msgid "Cut Layer (3.0+)"
msgstr "Retalla la capa (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:21
msgid "Cuts the whole layer rather than just the pixels."
msgstr "Retalla tota la capa en lloc de només els píxels."

#: ../../reference_manual/main_menu/layers_menu.rst:22
msgid "Copy Layer (3.0+)"
msgstr "Copia una capa (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:23
msgid "Copy the whole layer rather than just the pixels."
msgstr "Copia tota la capa en lloc de només els píxels."

#: ../../reference_manual/main_menu/layers_menu.rst:24
msgid "Paste Layer (3.0+)"
msgstr "Enganxa la capa (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:25
msgid "Pastes the whole layer if any of the top two actions have been taken."
msgstr ""
"Enganxa tota la capa si s'ha realitzat alguna de les dues accions principals."

#: ../../reference_manual/main_menu/layers_menu.rst:27
#: ../../reference_manual/main_menu/layers_menu.rst:41
#: ../../reference_manual/main_menu/layers_menu.rst:60
#: ../../reference_manual/main_menu/layers_menu.rst:78
#: ../../reference_manual/main_menu/layers_menu.rst:92
#: ../../reference_manual/main_menu/layers_menu.rst:102
#: ../../reference_manual/main_menu/layers_menu.rst:118
msgid "Organizes the following actions:"
msgstr "Organitza les següents accions:"

#: ../../reference_manual/main_menu/layers_menu.rst:29
msgid "Paint Layer"
msgstr "Capa de pintura"

#: ../../reference_manual/main_menu/layers_menu.rst:30
msgid "Add a new paint layer."
msgstr "Afegeix una capa de pintura nova."

#: ../../reference_manual/main_menu/layers_menu.rst:31
msgid "New layer from visible (3.0.2+)"
msgstr "Capa nova des de la visible (3.0.2+)"

#: ../../reference_manual/main_menu/layers_menu.rst:32
msgid "Add a new layer with the visible pixels."
msgstr "Afegeix una capa nova amb els píxels visibles."

#: ../../reference_manual/main_menu/layers_menu.rst:33
msgid "Duplicate Layer or Mask"
msgstr "Duplica la capa o màscara"

#: ../../reference_manual/main_menu/layers_menu.rst:34
msgid "Duplicates the layer."
msgstr "Duplica la capa."

#: ../../reference_manual/main_menu/layers_menu.rst:35
msgid "Cut Selection to New Layer"
msgstr "Retalla la selecció a una capa nova"

#: ../../reference_manual/main_menu/layers_menu.rst:36
msgid "Single action for cut+paste."
msgstr "Acció única per a retalla i enganxa."

#: ../../reference_manual/main_menu/layers_menu.rst:38
msgid "New"
msgstr "Nova"

#: ../../reference_manual/main_menu/layers_menu.rst:38
msgid "Copy Selection to New Layer"
msgstr "Copia la selecció a una capa nova"

#: ../../reference_manual/main_menu/layers_menu.rst:38
msgid "Single action for copy+paste."
msgstr "Acció única per a copia i enganxa."

#: ../../reference_manual/main_menu/layers_menu.rst:43
msgid "Save Layer or Mask"
msgstr "Desa la capa o màscara"

#: ../../reference_manual/main_menu/layers_menu.rst:44
msgid "Saves the Layer or Mask as a separate image."
msgstr "Desa la Capa o Màscara com a una imatge separada."

#: ../../reference_manual/main_menu/layers_menu.rst:45
msgid "Save Vector Layer as SVG"
msgstr "Desa la capa vectorial com a SVG"

#: ../../reference_manual/main_menu/layers_menu.rst:46
msgid "Save the currently selected vector layer as an SVG."
msgstr "Desa la capa vectorial seleccionada com a un SVG."

#: ../../reference_manual/main_menu/layers_menu.rst:47
msgid "Save Group Layers"
msgstr "Desa les capes de grup"

#: ../../reference_manual/main_menu/layers_menu.rst:48
msgid "Saves the top-level group layers as single-layer images."
msgstr ""
"Desa les capes de grup del nivell superior com a imatges d'una sola capa."

#: ../../reference_manual/main_menu/layers_menu.rst:49
msgid "Import Layer"
msgstr "Importa una capa"

#: ../../reference_manual/main_menu/layers_menu.rst:50
msgid "Import an image as a layer into the current file."
msgstr "Importa una imatge com a una capa al fitxer actual."

#: ../../reference_manual/main_menu/layers_menu.rst:52
msgid ""
"Import an image as a specific layer type. The following layer types are "
"supported:"
msgstr ""
"Importa una imatge com a un tipus específic de capa. S'admeten els següents "
"tipus de capa:"

#: ../../reference_manual/main_menu/layers_menu.rst:54
msgid "Paint layer"
msgstr "Capa de pintura"

#: ../../reference_manual/main_menu/layers_menu.rst:55
#: ../../reference_manual/main_menu/layers_menu.rst:66
msgid "Transparency Mask"
msgstr "Màscara de transparència"

#: ../../reference_manual/main_menu/layers_menu.rst:56
#: ../../reference_manual/main_menu/layers_menu.rst:68
msgid "Filter Mask"
msgstr "Màscara de filtratge"

#: ../../reference_manual/main_menu/layers_menu.rst:57
msgid "Import/Export"
msgstr "Importa/Exporta"

#: ../../reference_manual/main_menu/layers_menu.rst:57
msgid "Import as..."
msgstr "Importa com a..."

#: ../../reference_manual/main_menu/layers_menu.rst:57
#: ../../reference_manual/main_menu/layers_menu.rst:70
msgid "Selection Mask"
msgstr "Màscara de selecció"

#: ../../reference_manual/main_menu/layers_menu.rst:62
msgid "Convert a layer to..."
msgstr "Converteix una capa a..."

#: ../../reference_manual/main_menu/layers_menu.rst:64
msgid "Convert to Paint Layer"
msgstr "Converteix a una capa de pintura"

#: ../../reference_manual/main_menu/layers_menu.rst:65
msgid "Convert a mask or vector layer to a paint layer."
msgstr "Converteix una màscara o capa vectorial en una capa de pintura."

#: ../../reference_manual/main_menu/layers_menu.rst:67
msgid ""
"Convert a layer to a transparency mask. The image will be converted to "
"grayscale first, and these grayscale values are used to drive the "
"transparency."
msgstr ""
"Converteix una capa en una màscara de transparència. La imatge es convertirà "
"primer a escala de grisos, i aquests valors de l'escala de grisos "
"s'utilitzaran per a controlar la transparència."

#: ../../reference_manual/main_menu/layers_menu.rst:69
msgid ""
"Convert a layer to a filter mask. The image will be converted to grayscale "
"first, and these grayscale values are used to drive the filter effect area."
msgstr ""
"Converteix una capa en una màscara de filtratge. La imatge es convertirà "
"primer a escala de grisos, i aquests valors de l'escala de grisos "
"s'utilitzaran per a controlar l'àrea d'efecte del filtre."

#: ../../reference_manual/main_menu/layers_menu.rst:71
msgid ""
"Convert a layer to a selection mask. The image will be converted to "
"grayscale first, and these grayscale values are used to drive the selected "
"area."
msgstr ""
"Converteix una capa en una màscara de selecció. La imatge es convertirà "
"primer a escala de grisos, i aquests valors de l'escala de grisos "
"s'utilitzaran per a controlar l'àrea seleccionada."

#: ../../reference_manual/main_menu/layers_menu.rst:72
msgid "Convert Group to Animated Layer"
msgstr "Converteix un grup a una capa animada"

#: ../../reference_manual/main_menu/layers_menu.rst:73
msgid ""
"This takes the images in the group layer and makes them into frames of an "
"animated layer."
msgstr ""
"Pren les imatges a la capa de grup i les converteix en fotogrames d'una capa "
"animada."

#: ../../reference_manual/main_menu/layers_menu.rst:75
msgid "Convert Layer Color Space"
msgstr "Converteix l'espai de color de la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:75
msgid "This only converts the color space of the layer, not the image."
msgstr "Només converteix l'espai de color de la capa, no la imatge."

#: ../../reference_manual/main_menu/layers_menu.rst:80
msgid "All layers"
msgstr "Totes les capes"

#: ../../reference_manual/main_menu/layers_menu.rst:81
msgid "Select all layers."
msgstr "Selecciona totes les capes."

#: ../../reference_manual/main_menu/layers_menu.rst:82
msgid "Visible Layers"
msgstr "Capes visibles"

#: ../../reference_manual/main_menu/layers_menu.rst:83
msgid "Select all visible layers."
msgstr "Selecciona totes les capes visibles."

#: ../../reference_manual/main_menu/layers_menu.rst:84
msgid "Invisible Layers"
msgstr "Capes invisibles"

#: ../../reference_manual/main_menu/layers_menu.rst:85
msgid "Select all invisible layers, useful for cleaning up a sketch."
msgstr "Selecciona totes les capes invisibles, útil per a netejar un esbós."

#: ../../reference_manual/main_menu/layers_menu.rst:86
msgid "Locked Layers"
msgstr "Capes bloquejades"

#: ../../reference_manual/main_menu/layers_menu.rst:87
msgid "Select all locked layers."
msgstr "Selecciona totes les capes bloquejades."

#: ../../reference_manual/main_menu/layers_menu.rst:89
msgid "Select (3.0+):"
msgstr "Selecciona (3.0+):"

#: ../../reference_manual/main_menu/layers_menu.rst:89
msgid "Unlocked Layers"
msgstr "Capes desbloquejades"

#: ../../reference_manual/main_menu/layers_menu.rst:89
msgid "Select all unlocked layers."
msgstr "Selecciona totes les capes desbloquejades."

#: ../../reference_manual/main_menu/layers_menu.rst:94
msgid "Quick Group (3.0+)"
msgstr "Agrupa ràpid (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:95
msgid "Adds all selected layers to a group."
msgstr "Afegeix totes les capes seleccionades a un grup."

#: ../../reference_manual/main_menu/layers_menu.rst:96
msgid "Quick Clipping Group (3.0+)"
msgstr "Agrupa ràpid de retall (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:97
msgid ""
"Adds all selected layers to a group and adds a alpha-inherited layer above "
"it."
msgstr ""
"Afegeix totes les capes seleccionades a un grup i afegeix una capa amb "
"hereta l'alfa sobre seu."

#: ../../reference_manual/main_menu/layers_menu.rst:99
msgid "Group"
msgstr "Agrupa"

#: ../../reference_manual/main_menu/layers_menu.rst:99
msgid "Quick Ungroup"
msgstr "Desagrupa ràpid"

#: ../../reference_manual/main_menu/layers_menu.rst:99
msgid "Ungroups the activated layer."
msgstr "Desagrupa la capa activada."

#: ../../reference_manual/main_menu/layers_menu.rst:104
msgid "Mirror Layer Horizontally"
msgstr "Emmiralla la capa horitzontalment"

#: ../../reference_manual/main_menu/layers_menu.rst:105
msgid "Mirror the layer horizontally using the image center."
msgstr "Emmiralla la capa horitzontalment utilitzant el centre de la imatge."

#: ../../reference_manual/main_menu/layers_menu.rst:106
msgid "Mirror Layer Vertically"
msgstr "Emmiralla la capa verticalment"

#: ../../reference_manual/main_menu/layers_menu.rst:107
msgid "Mirror the layer vertically using the image center."
msgstr "Emmiralla la capa verticalment utilitzant el centre de la imatge."

#: ../../reference_manual/main_menu/layers_menu.rst:108
msgid "Rotate"
msgstr "Gira"

#: ../../reference_manual/main_menu/layers_menu.rst:109
msgid "Rotate the layer around the image center."
msgstr "Gira la capa al voltant del centre de la imatge."

#: ../../reference_manual/main_menu/layers_menu.rst:110
msgid "Scale Layer"
msgstr "Escala la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:111
msgid ""
"Scale the layer by the given amounts using the given interpolation filter."
msgstr ""
"Escala la capa per les quantitats indicades utilitzant el filtre "
"d'interpolació indicat."

#: ../../reference_manual/main_menu/layers_menu.rst:112
msgid "Shear Layer"
msgstr "Inclina la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:113
msgid "Shear the layer pixels by the given X and Y angles."
msgstr "Inclina els píxels de la capa pels angles X i Y indicats."

#: ../../reference_manual/main_menu/layers_menu.rst:115
msgid "Offset Layer"
msgstr "Desplaça la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:115
msgid "Offset the layer pixels by a given amount."
msgstr "Desplaça els píxels de la capa per una quantitat indicada."

#: ../../reference_manual/main_menu/layers_menu.rst:120
msgid "Split Alpha"
msgstr "Divideix l'alfa"

#: ../../reference_manual/main_menu/layers_menu.rst:121
msgid ""
"Split the image transparency into a mask. This is useful when you wish to "
"edit the transparency separately."
msgstr ""
"Divideix la transparència de la imatge en una màscara. Això és útil quan "
"voleu editar la transparència per separat."

#: ../../reference_manual/main_menu/layers_menu.rst:122
msgid "Split Layer"
msgstr "Divideix la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:123
msgid ":ref:`Split the layer <split_layer>` into given color fields."
msgstr ":ref:`Divideix la capa <split_layer>` en els camps de color indicats."

#: ../../reference_manual/main_menu/layers_menu.rst:125
msgid "Split..."
msgstr "Divideix..."

#: ../../reference_manual/main_menu/layers_menu.rst:125
msgid "Clones Array"
msgstr "Matriu de clonades"

#: ../../reference_manual/main_menu/layers_menu.rst:125
msgid ""
"A complex bit of functionality to generate clone-layers for quick sprite "
"making. See :ref:`clones_array` for more details."
msgstr ""
"Una funcionalitat complexa per a generar capes de clonades per a la "
"realització d'una franja ràpida. Per a més detalls, vegeu :ref:"
"`clones_array`."

#: ../../reference_manual/main_menu/layers_menu.rst:127
msgid "Edit Metadata"
msgstr "Edita les metadades"

#: ../../reference_manual/main_menu/layers_menu.rst:128
msgid "Each layer can have its own metadata."
msgstr "Cada capa podrà tenir les seves pròpies metadades."

#: ../../reference_manual/main_menu/layers_menu.rst:130
msgid "Shows a histogram."
msgstr "Mostra un histograma."

#: ../../reference_manual/main_menu/layers_menu.rst:134
msgid "Removed. Use the :ref:`histogram_docker` instead."
msgstr "S'ha eliminat. En el seu lloc utilitzeu l':ref:`histogram_docker`."

#: ../../reference_manual/main_menu/layers_menu.rst:136
msgid "Merge With Layer Below"
msgstr "Combina amb la capa inferior"

#: ../../reference_manual/main_menu/layers_menu.rst:137
msgid "Merge a layer down."
msgstr "Combina una capa inferior."

#: ../../reference_manual/main_menu/layers_menu.rst:138
msgid "Flatten Layer"
msgstr "Aplana la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:139
msgid "Flatten a Group Layer or flatten the masks into any other layer."
msgstr ""
"Aplana una capa de grup o aplana les màscares dins de qualsevol altra capa."

#: ../../reference_manual/main_menu/layers_menu.rst:140
msgid "Rasterize Layer"
msgstr "Rasteritza la capa"

#: ../../reference_manual/main_menu/layers_menu.rst:141
msgid "For making vectors into raster layers."
msgstr "Per a crear vectors en capes ràster."

#: ../../reference_manual/main_menu/layers_menu.rst:142
msgid "Flatten Image"
msgstr "Aplana la imatge"

#: ../../reference_manual/main_menu/layers_menu.rst:143
msgid "Flatten all layers into one."
msgstr "Aplana totes les capes en una sola."

#: ../../reference_manual/main_menu/layers_menu.rst:145
msgid "Set the PS-style layerstyle."
msgstr "Estableix l'estil de la capa a l'estil del PS."
