# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:23+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../reference_manual/popup-palette.rst:1
msgid "The Pop-up Palette in Krita"
msgstr "Palettrutan i Krita"

#: ../../reference_manual/popup-palette.rst:10
#: ../../reference_manual/popup-palette.rst:14
msgid "Pop-up Palette"
msgstr "Palettruta"

#: ../../reference_manual/popup-palette.rst:16
msgid ""
"The Pop-up Palette is a feature unique to Krita amongst the digital painting "
"applications. It is designed to increase productivity and save time of the "
"artists by providing quick access to some of the most frequently used tools "
"and features in Krita. The Pop-up palette can be accessed by |mouseright| on "
"the canvas. A circular palette similar to what is shown in the image below "
"will spawn at the position your mouse cursor."
msgstr ""
"Palettrutan är en unik funktion i Krita bland digitala målningsprogram. Den "
"är konstruerad för att öka produktiviteten och spara konstnärens tid genom "
"att tillhandahålla snabb åtkomst av vissa av de oftast använda verktygen och "
"funktionerna i Krita. Palettrutan kan kommas åt med ett högerklick på duken. "
"En cirkulär palett som liknar vad som visas nedan dyker upp vid muspekarens "
"position."

#: ../../reference_manual/popup-palette.rst:19
msgid ".. image:: images/popup-palette-detail.svg"
msgstr ".. image:: images/popup-palette-detail.svg"

#: ../../reference_manual/popup-palette.rst:20
msgid ""
"As shown in the image above, the pop-up palette has the following tools and "
"quick access shortcuts integrated into it"
msgstr ""
"Som visas på bilden ovan, har palettrutan följande verktyg och genvägar för "
"snabb åtkomst integrerade."

#: ../../reference_manual/popup-palette.rst:22
msgid ""
"Foreground color and Background color indicators on the top left of the "
"palette."
msgstr ""
"Indikering av förgrundsfärg och bakgrundsfärg längst upp till vänster om "
"paletten."

#: ../../reference_manual/popup-palette.rst:23
msgid ""
"A canvas rotation circular slider, which can help the artist quickly rotate "
"the canvas while painting."
msgstr ""
"Ett cirkulärt reglage för rotation av duken, som kan hjälpa konstnären att "
"snabbt rotera duken under målning."

#: ../../reference_manual/popup-palette.rst:24
msgid ""
"A group of brush presets, based on the tag selected by the artist. By "
"default the **My Favorite** tag is selected. By default only first 10 "
"presets from the tag are shown, however you can change the number of brush "
"presets shown by changing the value in the :ref:`Miscellaneous Settings "
"Section <misc_settings>` of the dialog box."
msgstr ""
"En grupp av penselförinställningar, baserad på etiketten vald av konstnären. "
"Normalt är etiketten **Mina favoriter** är vald. Bara de första 10 "
"förinställningarna från etiketten visas normalt, dock kan antal "
"penselförinställningarna som visas ändras genom att ändra värdet i "
"dialogrutans sektion :ref:`Diverse inställningar <misc_settings>`."

#: ../../reference_manual/popup-palette.rst:25
msgid ""
"Color Selector with which you can select the hue from the circular ring and "
"lightness and saturation from the triangular area in the middle."
msgstr ""
"Färgväljare som man kan använda för att välja färgton från den cirkulära "
"ringen och ljushet och färgmättnad från det triangulära området i mitten."

#: ../../reference_manual/popup-palette.rst:26
msgid ""
"Color history area shows the most recent color swatches that you have used "
"while painting."
msgstr ""
"Färghistorikområdet visar de senaste färgrutorna som man har använt vid "
"målning."

#: ../../reference_manual/popup-palette.rst:27
msgid ""
"The tag list for brush preset will show you the list of both custom and "
"default tags to choose from, selecting a tag from this list will show the "
"corresponding brush presets in the palette."
msgstr ""
"Etikettlistan över penselförinställningar visar listan med både egna och "
"standardetiketter att välja bland. Att välja en etikett i listan visar "
"motsvarande penselförinställningar i paletten."

#: ../../reference_manual/popup-palette.rst:28
msgid ""
"The common brush options such as size, opacity, angle et cetera will be "
"shown when you click the **>** icon. A dialog box will appear which will "
"have the sliders to adjust the brush options. You can choose which options "
"are shown in this dialog box by clicking on the settings icon."
msgstr ""
"De vanliga penselalternativen som storlek, ogenomskinlighet, vinkel, et "
"cetera, visas när man klickar på ikonen **>**. En dialogruta dyker upp som "
"har skjutreglagen för att justera penselalternativen. Man kan välja vilka "
"alternativ som visas i dialogrutan genom att klicka på inställningsikonen."

#: ../../reference_manual/popup-palette.rst:29
msgid "The zoom slider allows you to quickly zoom the canvas."
msgstr "Zoomreglaget låter dig snabbt zooma duken."

#: ../../reference_manual/popup-palette.rst:30
msgid "The 100% button sets the zoom to the 100% of the image size."
msgstr "Knappen 100 % ställer in zoomnivån till 100 % av bildstorleken."

#: ../../reference_manual/popup-palette.rst:31
msgid ""
"The button with the canvas icon switches to the canvas only mode, where the "
"toolbar and dockers are hidden."
msgstr ""
"Knappen med dukikon byter till enbart duk, där verktygsraden och panelerna "
"är dolda."

#: ../../reference_manual/popup-palette.rst:32
msgid ""
"The button with the mirror icon mirrors the canvas to help you spot the "
"errors in the painting."
msgstr ""
"Knappen med spegelikon speglar duken för att hjälpa till att upptäcka fel på "
"målningen."
