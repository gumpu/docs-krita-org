# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:44+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/file_svg.rst:1
msgid "The Scalable Vector Graphics file format in Krita."
msgstr "Filformatet Scalable Vector Graphic i Krita."

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "SVG"
msgstr "SVG"

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "*.svg"
msgstr "*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "Scalable Vector Graphics Format"
msgstr "Formatet Scalable Vector Graphic"

#: ../../general_concepts/file_formats/file_svg.rst:15
msgid "\\*.svg"
msgstr "\\*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:17
msgid ""
"``.svg``, or Scalable Vector Graphics, is the most modern vector graphics "
"interchange file format out there."
msgstr ""
"Scalable Vector Graphics eller ``.svg`` är det modernaste utbytesformatet "
"för vektorgrafik som är tillgängligt."

#: ../../general_concepts/file_formats/file_svg.rst:19
msgid ""
"Being vector graphics, SVG is very light weight. This is because it usually "
"only stores coordinates and parameters for the maths involved with vector "
"graphics."
msgstr ""
"Eftersom det är vektorgrafik är SVG mycket lättviktigt. Det beror på att det "
"oftast bara lagrar koordinater och parametrar för matematiken involverad med "
"vektorgrafik."

#: ../../general_concepts/file_formats/file_svg.rst:21
msgid ""
"It is maintained by the W3C SVG working group, who also maintain other open "
"standards that make up our modern internet."
msgstr ""
"Det underhålls av W3C SVG-arbetsgruppen, som också underhåller andra öppna "
"standarder som utgör vårt moderna Internet."

#: ../../general_concepts/file_formats/file_svg.rst:23
msgid ""
"While you can open up SVG files with any text-editor to edit them, it is "
"best to use a vector program like Inkscape. Krita 2.9 to 3.3 supports "
"importing SVG via the add shape docker. Since Krita 4.0, SVGs can be "
"properly imported, and you can export singlevector layers via :menuselection:"
"`Layer --> Import/Export --> Save Vector Layer as SVG...`. For 4.0, Krita "
"will also use SVG to save vector data into its :ref:`internal format "
"<file_kra>`."
msgstr ""
"Även om man kan öppna SVG-filer med vilken texteditor som helst för att "
"redigera dem, är det bäst att använda ett vektorprogram som Inkscape. Krita "
"2.9 till 3.3 stöder import av SVG via panelen Lägg till form. Sedan Krita "
"4.0 kan SVG:er importeras på riktigt, och man kan exportera enkelvektorlager "
"via :menuselection:`Layer --> Import/Export --> Save Vector Layer as SVG..."
"`. Med 4.0 använder Krita också SVG för att spara vektordata i det :ref:"
"`internal format <file_kra>`."

#: ../../general_concepts/file_formats/file_svg.rst:25
msgid ""
"SVG is designed for the internet, though sadly, because vector graphics are "
"considered a bit obscure compared to raster graphics, not a lot of websites "
"accept them yet. Hosting them on your own webhost works just fine though."
msgstr ""
"Även om SVG är konstruerat för Internet, accepterar tråkigt nog inte många "
"webbplatser ännu, eftersom vektorgrafik anses vara något obskyrt jämfört med "
"rastrerad grafik. Att tillhandahålla dem på ett eget webbhotell fungerar "
"dock alldeles utmärkt."
