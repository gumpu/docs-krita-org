# Translation of docs_krita_org_general_concepts___file_formats___lossy_lossless.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___lossy_lossless\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:37+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/lossy_lossless.rst:1
msgid "The difference between lossy and lossless compression."
msgstr "Відмінності між стисканням із втратами і стисканням без втрат."

#: ../../general_concepts/file_formats/lossy_lossless.rst:10
msgid "lossy"
msgstr "з втратами"

#: ../../general_concepts/file_formats/lossy_lossless.rst:10
msgid "lossless"
msgstr "без втрат"

#: ../../general_concepts/file_formats/lossy_lossless.rst:10
msgid "compression"
msgstr "стискання"

#: ../../general_concepts/file_formats/lossy_lossless.rst:17
msgid "Lossy and Lossless Image Compression"
msgstr "Стискання зображень із втратами і без втрат"

#: ../../general_concepts/file_formats/lossy_lossless.rst:20
msgid ""
"When we compress a file, we do this because we want to temporarily make it "
"smaller (like for sending over email), or we want to permanently make it "
"smaller (like for showing images on the internet)."
msgstr ""
"Коли ми стискаємо файл, ми робимо це через те, що хочемо тимчасово зменшити "
"його об'єм (наприклад, для надсилання електронною поштою), або через те, що "
"хочемо назавжди зробити його меншим (наприклад, для показу зображень у "
"інтернеті)."

#: ../../general_concepts/file_formats/lossy_lossless.rst:22
msgid ""
"*Lossless* compression techniques are for when we want to *temporarily* "
"reduce information. As the name implies, they compress without losing "
"information. In text, the use of abbreviations is a good example of a "
"lossless compression technique. Everyone knows 'etc.' expands to 'etcetera', "
"meaning that you can half the 8 character long 'etcetera' to the four "
"character long 'etc.'."
msgstr ""
"Методики стискання *без втрат* передбачають *тимчасове* зменшення об'єму "
"даних. Як можна зрозуміти з назви, стискання відбувається без втрати даних. "
"Зразком стискання без втрат є використання у тексті абревіатур. Усі знають, "
"що «див.» — скорочення від «дивись», у якому ми замість шести літер слова "
"«дивись» використовуємо стиснуту форму з чотирьох знаків — «див.»."

#: ../../general_concepts/file_formats/lossy_lossless.rst:24
msgid ""
"Within image formats, examples of such compression is by for example "
"'indexed' color, where we make a list of available colors in an image, and "
"then assign a single number to them. Then, when describing the pixels, we "
"only write down said number, so that we don't need to write the color "
"definition over and over."
msgstr ""
"У форматах зображень прикладом такого стискання є використання "
"«індексованих» кольорів: ми створюємо список усіх кольорів зображення, а "
"потім просто пов'язуємо із кожним з них одне число. Доті, при описі пікселів "
"нам достатньо вказати відповідне число — потреби у повному записі визначення "
"кольору вже немає."

#: ../../general_concepts/file_formats/lossy_lossless.rst:26
msgid ""
"*Lossy* compression techniques are for when we want to *permanently* reduce "
"the file size of an image. This is necessary for final products where having "
"a small filesize is preferable such as a website. That the image will not be "
"edited anymore after this allows for the use of the context of a pixel to be "
"taken into account when compressing, meaning that we can rely on "
"psychological and statistical tricks."
msgstr ""
"Методики стискання *із втратами* передбачають зменшення розмірів файла "
"зображення *без можливості відновлення початкової якості*. Таке стискання є "
"необхідним для остаточних продуктів, для яких важливим є розмір файла, "
"зокрема для сайтів. Припущення про те, що редагування зображення більше не "
"відбуватиметься, надає змогу скористатися контекстом кожного пікселя для "
"стискання шляхом використання психологічних та статистичних трюків."

#: ../../general_concepts/file_formats/lossy_lossless.rst:28
msgid ""
"One of the primary things JPEG for example does is chroma sub-sampling, that "
"is, to split up the image into a grayscale and two color versions (one "
"containing all red-green contrast and the other containing all blue-yellow "
"contrast), and then it makes the latter two versions smaller. This works "
"because humans are much more sensitive to differences in lightness than we "
"are to differences in hue and saturation."
msgstr ""
"Наприклад, однією із основних речей, які виконуються під час стискання у "
"форматі JPEG, є субдискретизація за насиченістю, тобто поділ зображення на "
"зображення у відтінках сірого та дві кольорові версії (одну із даними щодо "
"усіх контрастностей червоний-зелений, а іншу для усіх контрастностей синій-"
"жовтий), із наступним зменшенням розмірів кольорових версій. Практичною "
"основою можливості такого стискання є те, що людське око є набагато "
"чутливішим до різниці в освітленості, ніж до різниці у відтінках і "
"насиченості."

#: ../../general_concepts/file_formats/lossy_lossless.rst:30
msgid ""
"Another thing it does is to use cosine waves to describe contrasts in an "
"image. What this means is that JPEG and other lossy formats using this are "
"*very good at describing gradients, but not very good at describing sharp "
"contrasts*."
msgstr ""
"Ще одним перетворенням є використання синусоїд для опису контрастності на "
"зображенні. Це означає, що JPEG та інші формати зберігання даних з втратами, "
"де використано ту саму методику, *добре працюють у описі поступових "
"переходів між кольорами, але призводять до появи дефектів на ділянках із "
"різкими контрастами*."

#: ../../general_concepts/file_formats/lossy_lossless.rst:32
msgid ""
"Conversely, lossless image compression techniques are *really good at "
"describing images with few colors thus sharp contrasts, but are not good to "
"compress images with a lot of gradients*."
msgstr ""
"І навпаки, методики стискання зображень без втрат є *дуже добрими для "
"зберігання зображень, на яких є небагато кольорів, отже багато різких "
"контрастів, але малопридатні для зберігання зображень із багатьма "
"градієнтами*."

#: ../../general_concepts/file_formats/lossy_lossless.rst:34
msgid ""
"Another big difference between lossy and lossless images is that lossy file "
"formats will degrade if you re-encode them, that is, if you load a JPEG into "
"Krita edit a little, resave, edit a little, resave, each subsequent save "
"will lose some data. This is a fundamental part of lossy image compression, "
"and the primary reason we use working files."
msgstr ""
"Це однією визначною відмінністю між зображеннями із втратою якості та без "
"втрати якості є те, що якість зображення у форматах із втратою якості "
"погіршується після перепаковування. Тобто, якщо ви завантажите JPEG до "
"Krita, виконаєте незначне редагування, збережете дані, знову виконаєте "
"редагування, ще раз збережете дані… кожного разу частина даних "
"втрачатиметься. Це фундаментальна особливість стискання зображень з втратами "
"і основна причина для використання для проміжного зберігання даних "
"особливого формату."

#: ../../general_concepts/file_formats/lossy_lossless.rst:38
msgid ""
"If you're interested in different compression techniques, `Wikipedia's "
"page(s) on image compression <https://en.wikipedia.org/wiki/"
"Image_compression>`_ are very good, if not a little technical."
msgstr ""
"Якщо ви цікавитеся різними методиками стискання даних, добрим джерелом даних "
"є `стаття у Вікіпедії щодо стискання зображень <https://en.wikipedia.org/"
"wiki/Image_compression>`_, хоча її вміст і є дещо технічним."
